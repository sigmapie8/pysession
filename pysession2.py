'''
functions : give something get something

def function_name(arg1, arg2):
    some_variable = arg1 + arg2
    return some_variable

function_name(2, 3)

def function_name(arg1, arg2=3):
    some_variable = arg1+arg2
    return some_variable
    
print(function_name(6))
print(function_name(6, 4))
#try implementing the arguments backwards

def function_name(arg1, arg2=3):
    some_variable = arg1+arg2
    return 4
    
print(function_name(2))

def function_name(arg1, arg2=3):
    some_variable = arg1+arg2
    return
    
print(function_name(2))
#erase return and run
'''
