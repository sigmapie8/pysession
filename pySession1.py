# type() function, shows the type class of the input.
'''
num = int(input())

if(num%2 == 0):
    print("And the number is:")
    print("Even")
print("If block ends here")

if(num%2 == 0):
    print("And the number is:")
    print("Even")
else:
    print("And the number is:")
    print("Odd")
    
#lucky Number
if( num < 20):
    print("not a lucky number")
elif( num >= 20 and num num <= 40):
    print("lucky number")
else:
    print("not a lucky number")
    
    
#loops

for i in range(1, 10):
    print(i)

for i in range(10):
    print(i)
    
for i in range(0, 10, 2):
    print(i)
    
for i in range(10, 0, -1):
    print(i)

for j in range(100):
    print("Manav ")
    
#can you do this without loop?

i = 0
while(i<10):
    print(i)
    i += 1
#Note that there is no i++ in python


#Strings

string1 = "Hi, Avengers Endgame is going 2 be Awesome!"
print(string1)
print(string1[2]) # prints the 2nd index character of the string
print(string1[0:3]) #Hi, - prints 0,1,2 index characters of the string
print(string1[0:4]) #Hi,  - space is also part of string
print(string1) # string slicing does not change the original string
print(string1[5:]) #prints everything after 5th character 
print(string1.upper()) #converts all the character to uppercase alphabets
print(string1) #using string functions does not change the original string
print(string1.lower()) #converts all the character to lowercase alphabets

for alphabet in string1:
    print(alphabet)

for alphabet in string1:
    print(alphabet, end="")
print()

print(string1.split(" "))
'''

'''
#lists

lis = []
lis.append("text")
print(lis)
lis.append(2.05) #adds element at the end of the list
lis.append(81)
print(lis)
lis.insert(1, 23) #insert element at the given index
print(lis)
lis.pop() #removes last element from the list
print(lis)
lis.remove(23) #removes the first instance of any given number
print(lis)
lis[1] = 20 #edits an existing element of the list
print(lis)

#traversing on a list
for el in lis:
    print(el)
    
#traversing on a list by index
for index in range(length(lis)):
    print(lis[index])

#Slicing also works for lists. Try it.

lis1 = [1, 2, 3, 4, 5]
lis2 = lis1
lis1.pop()
print(lis2)

lis2 = lis1[:]
'''

'''
#dictionaries
in 3.6 dictionaries do not order preserve. In 3.7 they do.

d = dict()
d['one'] = 1
d[2] = 'two'
d[3] = 3

del d[3]

for i in d:
    print(d[i])
    
for i in d.values():
    print(i)
    
for i in d.items():
    key, value = i
    print(key, value)


'''